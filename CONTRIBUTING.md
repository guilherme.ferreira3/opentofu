# Contributing

Please read about CI/CD components and best practices at: https://docs.gitlab.com/ee/ci/components

## README

The [`README.md`](README.md) file is generated using `make docs` (see [`Makefile`](Makefile))
from [`.gitlab/README.md.template`](.gitlab/README.md.template).

## Upgrade OpenTofu versions

When adding new OpenTofu versions the following places need to be updated:

| File                                                         | What                                                                                      |
| ------------------------------------------------------------ | ----------------------------------------------------------------------------------------- |
| [`templates/full-pipeline.yml`](templates/full-pipeline.yml) | The `default` value and `options` list of the `sepc.inputs.opentofu_versions` entry.      |
| [`.gitlab-ci.yml`](.gitlab-ci.yml)                           | The `.opentofu_versions.parallel.matrix` list and the `LATEST_OPENTOFU_VERSION` variable. |

All of the above definitions have to match each other.
We currently need to change it in multiple places, because there is not a good way to share information
from the templates and the components pipeline defintion - at least in the features we'd like to use them.

## Releasing

Use the `make release` command with the `VERSION` argument set to the
version of the new release, like: `make release VERSION=1.0.0`.
The version must adhere to the [Semantic Versioning](https://semver.org) format.
