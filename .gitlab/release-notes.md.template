Release $CI_COMMIT_TAG of components repository $CI_PROJECT_PATH.

See the [CHANGELOG](CHANGELOG.md) for what changes happened in this release.

## Usage

You can use the OpenTofu CI/CD component from the CI/CD catalog using:

```yaml
include:
  - component: $CI_SERVER_FQDN/$CI_PROJECT_PATH/full-pipeline@$CI_COMMIT_TAG
    inputs:
      # The version must currently be specified explicitly as an input,
      # to find the correctly associated images. # This can be removed
      # once https://gitlab.com/gitlab-org/gitlab/-/issues/438275 is solved.
      version: $CI_COMMIT_TAG
      opentofu_version: $LATEST_OPENTOFU_VERSION

stages: [validate, test, build, deploy, cleanup]
```

See the [README.md](README.md) for more usage examples.

## Available OpenTofu versions

This release is available with the following OpenTofu versions:

$AVAILABLE_OPENTOFU_VERSIONS

## Available GitLab OpenTofu images

This release deployed the following `gitlab-opentofu` images:

$AVAILABLE_IMAGES

> **Note:**
>
> When using the component with the inputs `version` and `opentofu_version`,<br>
> then the image tag is constructed with the following pattern:<br>
> `<version>-opentofu<opentofu_version>`
>
> Read more about versioning and releases [here](README.md#releases-versioning).
