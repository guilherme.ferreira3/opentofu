spec:
  inputs:
    # Stages
    stage_validate:
      default: 'validate'
      description: 'Defines the validate stage. This stage includes the `fmt` and `validate` jobs.'
    stage_test:
      default: 'test'
      description: 'Defines the test stage. This stage includes the `test` job.'
    stage_build:
      default: 'build'
      description: 'Defines the build stage. This stage includes the `plan` job.'
    stage_deploy:
      default: 'deploy'
      description: 'Defines the deploy stage. This stage includes the `apply` job.'
    stage_cleanup:
      default: 'cleanup'
      description: 'Defines the cleanup stage. This stage includes the `destroy` and `delete-state` jobs.'

    # Versions
    # This version is only required, because we cannot access the context of the component,
    # see https://gitlab.com/gitlab-org/gitlab/-/issues/438275
    version:
      default: 'latest'
      description: 'Version of this component. Has to be the same as the one in the component include entry.'

    opentofu_version:
      default: '1.7.1'
      options:
        - '$OPENTOFU_VERSION'
        - '1.7.1'
        - '1.7.0'
        - '1.7.0-alpha1'
        - '1.6.2'
        - '1.6.1'
        - '1.6.0'
      description: 'OpenTofu version that should be used.'

    # Images
    image_registry_base:
      default: '$CI_REGISTRY/components/opentofu'
      description: 'Host URI to the job images. Will be combined with `image_name` to construct the actual image URI.'
    # FIXME: not yet possible because of https://gitlab.com/gitlab-org/gitlab/-/issues/438722
    # gitlab_opentofu_image:
    #   # FIXME: This should reference the component tag that is used.
    #   #        Currently, blocked by https://gitlab.com/gitlab-org/gitlab/-/issues/438275
    #   # default: '$CI_REGISTRY/components/opentofu/gitlab-opentofu:$[[ inputs.opentofu_version ]]'
    #   default: '$CI_REGISTRY/components/opentofu/gitlab-opentofu:$[[ inputs.version ]]-opentofu$[[ inputs.opentofu_version ]]'
    #   description: 'Tag of the gitlab-opentofu image.'

    image_name:
      default: 'gitlab-opentofu'
      description: 'Image name for the job images. Hosted under `image_registry_base`.'

    # Configuration
    job_name_prefix:
      default: '.opentofu:'
      description: 'Prefix for all job names. Jobs will be hidden if leading with a dot `.`.'
    root_dir:
      default: ${CI_PROJECT_DIR}
      description: 'Root directory for the OpenTofu project.'
    state_name:
      default: default
      description: 'Remote OpenTofu state name.'
    auto_apply:
      default: false
      type: boolean
      description: 'Whether the apply job is manual or automatically run.'
    auto_destroy:
      default: false
      type: boolean
      description: 'Whether the destroy job is manual or automatically run.'

---

include:
  - local: '/templates/fmt.yml'
    inputs:
      as: '$[[ inputs.job_name_prefix ]]fmt'
      stage: $[[ inputs.stage_validate ]]
      version: $[[ inputs.version ]]
      opentofu_version: $[[ inputs.opentofu_version ]]
      image_registry_base: $[[ inputs.image_registry_base ]]
      image_name: $[[ inputs.image_name ]]
      root_dir: $[[ inputs.root_dir ]]
  - local: '/templates/validate.yml'
    inputs:
      as: '$[[ inputs.job_name_prefix ]]validate'
      stage: $[[ inputs.stage_validate ]]
      version: $[[ inputs.version ]]
      opentofu_version: $[[ inputs.opentofu_version ]]
      image_registry_base: $[[ inputs.image_registry_base ]]
      image_name: $[[ inputs.image_name ]]
      root_dir: $[[ inputs.root_dir ]]
      state_name: $[[ inputs.state_name ]]
  - local: '/templates/test.yml'
    inputs:
      as: '$[[ inputs.job_name_prefix ]]test'
      stage: $[[ inputs.stage_test ]]
      version: $[[ inputs.version ]]
      opentofu_version: $[[ inputs.opentofu_version ]]
      image_registry_base: $[[ inputs.image_registry_base ]]
      image_name: $[[ inputs.image_name ]]
      root_dir: $[[ inputs.root_dir ]]
      state_name: $[[ inputs.state_name ]]
  - local: '/templates/plan.yml'
    inputs:
      as: '$[[ inputs.job_name_prefix ]]plan'
      stage: $[[ inputs.stage_build ]]
      version: $[[ inputs.version ]]
      opentofu_version: $[[ inputs.opentofu_version ]]
      image_registry_base: $[[ inputs.image_registry_base ]]
      image_name: $[[ inputs.image_name ]]
      root_dir: $[[ inputs.root_dir ]]
      state_name: $[[ inputs.state_name ]]
  - local: '/templates/apply.yml'
    inputs:
      as: '$[[ inputs.job_name_prefix ]]apply'
      stage: $[[ inputs.stage_deploy ]]
      version: $[[ inputs.version ]]
      opentofu_version: $[[ inputs.opentofu_version ]]
      image_registry_base: $[[ inputs.image_registry_base ]]
      image_name: $[[ inputs.image_name ]]
      root_dir: $[[ inputs.root_dir ]]
      state_name: $[[ inputs.state_name ]]
      auto_apply: $[[ inputs.auto_apply ]]
  - local: '/templates/destroy.yml'
    inputs:
      as: '$[[ inputs.job_name_prefix ]]destroy'
      stage: $[[ inputs.stage_cleanup ]]
      version: $[[ inputs.version ]]
      opentofu_version: $[[ inputs.opentofu_version ]]
      image_registry_base: $[[ inputs.image_registry_base ]]
      image_name: $[[ inputs.image_name ]]
      root_dir: $[[ inputs.root_dir ]]
      state_name: $[[ inputs.state_name ]]
      auto_destroy: $[[ inputs.auto_destroy ]]
  - local: '/templates/delete-state.yml'
    inputs:
      as: '$[[ inputs.job_name_prefix ]]delete-state'
      stage: $[[ inputs.stage_cleanup ]]
      state_name: $[[ inputs.state_name ]]
